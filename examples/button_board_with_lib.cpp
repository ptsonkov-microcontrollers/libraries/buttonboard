#include <Arduino.h>
#include <ButtonBoard.h>

byte analogPin = A5;
int reactionTime = 100;
int currentButtonID = 0;              // Initial button state (0 mean unpressed)
int reactionCounter = 0;              // Wait counter before to announce state change
int waitToReact = 250;                // Wait time (in miliseconds) before reaction
int buttonID;

// Change option:
// false - Normal operation
// true  - Read raw button values
bool DBG = false;

// 2D array with button values (buttonID,LOW,HIGH)
// To change values use debug function "button.valueDBG()"
// after create "button" object (check below)
int buttonValues[22][3] = {
	{0, 0, 0}, // button released
	{1, 316, 317}, // button 1
	{2, 203, 204}, // button 2
	{3, 129, 130}, // button 3
	{4, 46, 48}, // button 4
	{5, 18, 19}, // button 5
	{6, 11, 12}, // button 6
	{7, 421, 421}, // button 1 + button 2
	{8, 382, 382}, // button 1 + button 3
	{9, 340, 341}, // button 1 + button 4
	{10, 326, 327}, // button 1 + button 5
	{11, 323, 323}, // button 1 + button 6
	{12, 290, 291}, // button 2 + button 3
	{13, 235, 237}, // button 2 + button 4
	{14, 217, 218}, // button 2 + button 5
	{15, 212, 213}, // button 2 + button 6
	{16, 167, 168}, // button 3 + button 4
	{17, 146, 147}, // button 3 + button 5
	{18, 140, 141}, // button 3 + button 6
	{19, 66, 67}, // button 4 + button 5
	{20, 58, 60}, // button 4 + button 6
	{21, 31, 32} // button 5 + button 6
};

// Create button object
ButtonBoard button(analogPin, buttonValues);

// Forward function declaration
void Action(int btnID);

void setup() {

	Serial.begin(9600);

}

void loop() {

	if (DBG == true) {

		button.valueDBG();

	} else {

		buttonID = button.setButton();

		// Check if there is change in button ID since last check
		if (buttonID != currentButtonID) {
			reactionCounter++;
			if (reactionCounter == waitToReact) {
				currentButtonID = buttonID;
				reactionCounter = 0;
				Action(currentButtonID);
			}
		} else {
			reactionCounter = 0;
		}

	}
}

void Action(int btnID) {

	if(btnID == 0) {
		Serial.println(F("Button released"));
	} else if(btnID == 1) {
		Serial.println(F("Button1"));
	} else if(btnID == 2) {
		Serial.println(F("Button2"));
	} else if(btnID == 3) {
		Serial.println(F("Button3"));
	} else if(btnID == 4) {
		Serial.println(F("Button4"));
	} else if(btnID == 5) {
		Serial.println(F("Button5"));
	} else if(btnID == 6) {
		Serial.println(F("Button6"));
	} else if(btnID == 7) {
		Serial.println(F("Button1 + Button2"));
	} else if(btnID == 8) {
		Serial.println(F("Button1 + Button3"));
	} else if(btnID == 9) {
		Serial.println(F("Button1 + Button4"));
	} else if(btnID == 10) {
		Serial.println(F("Button1 + Button5"));
	} else if(btnID == 11) {
		Serial.println(F("Button1 + Button6"));
	} else if(btnID == 12) {
		Serial.println(F("Button2 + Button3"));
	} else if(btnID == 13) {
		Serial.println(F("Button2 + Button4"));
	} else if(btnID == 14) {
		Serial.println(F("Button2 + Button5"));
	} else if(btnID == 15) {
		Serial.println(F("Button2 + Button6"));
	} else if(btnID == 16) {
		Serial.println(F("Button3 + Button4"));
	} else if(btnID == 17) {
		Serial.println(F("Button3 + Button5"));
	} else if(btnID == 18) {
		Serial.println(F("Button3 + Button6"));
	} else if(btnID == 19) {
		Serial.println(F("Button4 + Button5"));
	} else if(btnID == 20) {
		Serial.println(F("Button4 + Button6"));
	} else if(btnID == 21) {
		Serial.println(F("Button5 + Button6"));
	}
}
